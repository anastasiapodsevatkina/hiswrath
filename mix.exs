defmodule Hiswrath.Mixfile do
  use Mix.Project

  def project do
    [app: :hiswrath,
     escript: escript_config,
     version: "0.0.1",
     elixir: "~> 1.1-dev",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  # Configuration for the OTP application
  def application do
    [applications: [:logger]]
  end

  # Dependencies
  defp deps do
    []
  end

  defp escript_config do
    [ main_module: Hiswrath.CLI ]
  end
end
