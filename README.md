# Hiswrath

**HisWrath is an academic tool for stress and load testing of network
application, server clusters and databases**

## Installation

  1. Add hiswrath to your list of dependencies in mix.exs:

        def deps do
          [{:hiswrath, "~> 0.0.1"}]
        end

  2. Ensure hiswrath is started before your application:

        def application do
          [applications: [:hiswrath]]
        end

## TODO list
    1. Add more GNU-friendly cli option parser
    2. Add HTTPoison for working with HTTP\HTTPS
    3. Look through JSON parsers (or write own)
    4. Add Ecto for creating DB quieries
    5. Add logging (maybe custom logger?)
    6. Create list of macroses for testing scenarious
    7. Add Websocket support
    8. Add behaviour for http-based protocols
    9. Add behaviour for db (add Ecto!!!)
    10. Move env options to Mix.Config
    11. Format default output in table
