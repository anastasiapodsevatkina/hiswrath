defmodule CliTest do
  use ExUnit.Case
  doctest Hiswrath

  import Hiswrath.CLI, only: [ parse_args: 1]

  test ":help returned with -h or --help options" do
    assert parse_args(["-h",     "anything"]) == :help
    assert parse_args(["--help", "anything"]) == :help
  end
end
