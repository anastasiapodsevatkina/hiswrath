defmodule Hiswrath.CLI do

  @default_env "dev"
  @moduledoc """
  Handling commandline parsing
  and results output in JSON
  """

  def main(argv) do
    argv
      |> parse_args
      |> process
  end

  @doc """
  `argv` can be:
  -h or --help -> describe switches
  TODO: add switch for json output
  TODO: what Hiswrath should do if run
  without switches?
  TODO: add switching between envs (dev, stage, prod)
  """
  def parse_args(argv) do
    parse = OptionParser.parse(argv, switches: [help: :boolean],
                                      aliases:  [h:    :help])
    case parse do
      { [help: true], _, _ } -> :help
      _                      -> :help
    end
  end


  def process(:help) do
    IO.puts """
    usage: hiswrath <type of output> [ type of environment | #{@default_env} ]
    """
    System.halt(0)
  end
end
